//
//  Coordinate.swift
//  Stack-in-Swift
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Cocoa

class Coordinate: NSObject {
    private var x: Int = 0
    private var y: Int = 0
    
    override var description: String {
        return "(\(x), \(y))"
    }
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}
