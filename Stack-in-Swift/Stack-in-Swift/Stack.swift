//
//  Stack.swift
//  Stack-in-Swift
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Cocoa

class Stack<T>: NSObject {

    private var m_pStack: [T] = []
    private var m_iStackCapacity: Int = 0
    
    private var m_iTop: Int = 0
    
    init(stackCapacity: Int) {
        m_iStackCapacity = stackCapacity

        m_iTop = 0
    }
    
    func clearStack() {
        m_iTop = 0
    }
    
    func isEmpty() -> Bool {
        return m_iTop == 0
    }
    
    func isFull() -> Bool {
        return m_iTop >= m_iStackCapacity
    }
    
    func push(_ element: T) -> Bool {
        if isFull() {
            return false
        }
        
        m_pStack.append(element)
        m_iTop += 1
        return true
    }
    
    func pop() -> (Bool, T?) {
        if isEmpty() {
            return (false, nil)
        }
        
        m_iTop -= 1
        let e = m_pStack[m_iTop]
        return (true, e)
    }
    
    func stackTraverse() {
        for i in stride(from: m_iTop-1, through: 0, by: -1) {
            print(m_pStack[i], separator: "", terminator: " ")
        }
        print("")
    }
}
