//
//  main.swift
//  Stack-in-Swift
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

import Foundation

func testIntStack() {
    let s = Stack<Int>(stackCapacity: 5)
    
    var res = s.push(2)
    res = s.push(5)
    
    s.stackTraverse()
    
    var e = s.pop()
    if e.0 {
        print(e.1!)
    }
    
    res = s.push(7)
    res = s.push(12)
    res = s.push(19)
    res = s.push(31)
    
    s.stackTraverse()
    
    e = s.pop()
    e = s.pop()
    e = s.pop()
    e = s.pop()
    e = s.pop()
    e = s.pop()
    
    s.stackTraverse()
    
    res = s.push(50)
    
    s.clearStack()
    
    if res {
        s.stackTraverse()
    }
}

func testObjectStack() {
    let s = Stack<Coordinate>(stackCapacity: 5)
    
    var res = s.push(Coordinate(x: 2, y: 2))
    res = s.push(Coordinate(x: 5, y: 5))
    
    s.stackTraverse()
    
    var e = s.pop()
    if e.0 {
        print(e.1!)
    }
    
    res = s.push(Coordinate(x: 7, y: 7))
    res = s.push(Coordinate(x: 12, y: 12))
    res = s.push(Coordinate(x: 19, y: 19))
    res = s.push(Coordinate(x: 31, y: 31))
    
    s.stackTraverse()
    
    e = s.pop()
    e = s.pop()
    e = s.pop()
    e = s.pop()
    e = s.pop()
    e = s.pop()
    
    s.stackTraverse()
    
    res = s.push(Coordinate(x: 50, y: 50))
    
    s.clearStack()
    
    if res {
        s.stackTraverse()
    }
}

print("testIntStack")
testIntStack()
print("testObjectStack")
testObjectStack()
